# node-ctsx

Just a small tool to not create new tsx components with hands

## Install

Install node and yarn

Clone repo, go to it's directory, give needed rights to files in it and add it's path to `$PATH`

```Bash
   git clone https://codeberg.org/hexane911/node-ctsx.git;
   cd node-ctsx;
   chmod +x *;
   yarn;
   ./add_to_path.sh ;
```

## Usage

Run `ctsx -h` to see input format

There's not much things to understand

Have a good day!